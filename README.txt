WASD: Move
Left click: Fire weapon
1: Swap to pistol (the purple box)
2: Swap to shotgun (the white cylinder. note that it must be picked up first!)
3: Swap to chaingun (the white cube. note that it must be picked up first!)

Music and sound effects are from Quake 2, XCOM: Enemy Unknown, and Half-Life.